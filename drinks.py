# drinks.py
drink_list = [
	{
		"name": "Rum & Coke",
		"ingredients": [
			"rum",
			"coke"
		],
                "alcIngCount": 1
	}, {
		"name": "Gin & Tonic",
		"ingredients": [
			"gin",
			"tonic"
		],
                "alcIngCount": 1
	}, {
		"name": "Long Island",
		"ingredients": [
			"gin",
			"rum",
			"vodka",
			"tequila",
			"coke",
			"oj"
		],
                "alcIngCount": 4
	}, {
		"name": "Screwdriver",
		"ingredients": [
			"vodka",
			"oj"
		],
                "alcIngCount": 1
	}, {
		"name": "Margarita",
		"ingredients": [
			"tequila",
			"mmix"
		],
                "alcIngCount": 1
	}, {
		"name": "Gin & Juice",
		"ingredients": [
			"gin",
			"oj"
		],
                "alcIngCount": 1
	}, {
		"name": "Tequila Sunrise",
		"ingredients": [
			"tequila",
			"oj"
		],
                "alcIngCount": 1
	}
]

drink_options = [
	{"name": "Gin", "value": "gin", "alc": True},
	{"name": "Rum", "value": "rum", "alc": True},
	{"name": "Vodka", "value": "vodka", "alc": True},
	{"name": "Tequila", "value": "tequila", "alc": True},
	{"name": "Tonic Water", "value": "tonic", "alc": False},
	{"name": "Coke", "value": "coke", "alc": False},
	{"name": "Orange Juice", "value": "oj", "alc": False},
	{"name": "Margarita Mix", "value": "mmix", "alc": False}
]
