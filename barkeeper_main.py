import time
import sys
import RPi.GPIO as GPIO
import json
import threading
import traceback

import Adafruit_SSD1306

import subprocess

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

from menu import MenuItem, Menu, Back, MenuContext, MenuDelegate
from drinks import drink_list, drink_options
from pump import pump
from lightning import Lightning

GPIO.setmode(GPIO.BCM)

LEFT_BTN_PIN = 5
LEFT_PIN_BOUNCE = 500

RIGHT_BTN_PIN = 13
RIGHT_PIN_BOUNCE = 500
RIGHT_PIN_BOUNCE_SEC = RIGHT_PIN_BOUNCE / 1000
MAX_AMOUNT = 300
MAX_ALC = 70

FLOW_RATE = 9.10/100.0

class Bartender(MenuDelegate): 
	def __init__(self):
		self.running = False
		self.cancelDrink = False
		self.selectedDrink = 0
		self.selectedAmount = 0
		self.selectedAlcShare = 0
		self.lastCallTime = 0

		self.lightning = Lightning()
		
		# Raspberry Pi pin configuration:
		RST = None     # on the PiOLED this pin isnt used
		
		# 128x64 display with hardware I2C:
		self.disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)
		
		# Load default font.
		self.fontSize = 15
		self.font = ImageFont.truetype('Timeless-Bold.ttf', self.fontSize)

                # init paddings
		self.paddingX = 15
		self.paddingXMiddle = 40
		self.paddingOneLineY = 25
		self.paddingThreeLinesY = 9
		
		# Initialize library.
		self.disp.begin()
		
		# Create blank image for drawing.
		# Make sure to create image with mode '1' for 1-bit color.
		self.image = Image.new('1', (self.disp.width, self.disp.height))
		
		# get pixel object
		self.pixelObject = self.image.load()
		
		# Get drawing object to draw on image.
		self.draw = ImageDraw.Draw(self.image)

	        # init buttons
		self.btn1Pin = LEFT_BTN_PIN
		self.btn2Pin = RIGHT_BTN_PIN
		
		# configure buttons
		GPIO.setup(self.btn1Pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		GPIO.setup(self.btn2Pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
  
		# load the valve configuration from file
		self.valve_configuration = Bartender.readValveConfiguration()
		for valve in self.valve_configuration.keys():
			GPIO.setup(self.valve_configuration[valve]["pin"], GPIO.OUT, initial=GPIO.HIGH)
		
		# load the pump configuration from file
		GPIO.setup(pump["pin"], GPIO.OUT, initial=GPIO.HIGH)

		print ("Done initializing")								

	@staticmethod
	def readValveConfiguration():
		return json.load(open('valve_config.json'))

	@staticmethod
	def writeValveConfiguration(configuration):
		with open("valve_config.json", "w") as jsonFile:
			json.dump(configuration, jsonFile)

	def startInterrupts(self):
		GPIO.add_event_detect(self.btn1Pin, GPIO.FALLING, callback=self.left_btn, bouncetime=LEFT_PIN_BOUNCE)  
		GPIO.add_event_detect(self.btn2Pin, GPIO.FALLING, callback=self.right_btn, bouncetime=RIGHT_PIN_BOUNCE)  

	def stopInterrupts(self):
		GPIO.remove_event_detect(self.btn1Pin)
		GPIO.remove_event_detect(self.btn2Pin)

	def buildMenu(self, drink_list, drink_options):
		# create a new main menu
		m = Menu("Main Menu")

		# add drink options
		drink_opts = []
		for d in drink_list:
			drink_opts.append(MenuItem('drink', d["name"], {"ingredients": d["ingredients"], "alcIngCount": d["alcIngCount"]}))

		configuration_menu = Menu("Konfigurieren")

		# add valve configuration options
		valve_opts = []
		for v in sorted(self.valve_configuration.keys()):
			if (v != "air_valve"):
				config = Menu(self.valve_configuration[v]["name"])
				# add fluid options for each valve
				for opt in drink_options:
					# star the selected option
					selected = "*" if opt["value"] == self.valve_configuration[v]["value"] else ""
					config.addOption(MenuItem('valve_selection', opt["name"], {"key": v, "value": opt["value"], "name": opt["name"]}))
				# add a back button so the user can return without modifying
				config.addOption(Back("Zurueck"))
				config.setParent(configuration_menu)
				valve_opts.append(config)

		# add valve menus to the configuration menu
		configuration_menu.addOptions(valve_opts)
		# adds an option that cleans all valves to the configuration menu
		configuration_menu.addOption(MenuItem('clean', 'Putzen'))
		# add a back button to the configuration menu
		configuration_menu.addOption(Back("Zurueck"))
		configuration_menu.setParent(m)

		# build order Menu
		self.amountMenu = Menu("Amount menu", visible = False)
		amount = 100
		while (amount <= MAX_AMOUNT):
			self.amountMenu.addOption(MenuItem("amount", str(amount) + " ml", amount))
			amount = amount + 100
		self.amountMenu.addOption(Back("Abbrechen"))
		self.amountMenu.setParent(m)

		# build alc menu
		self.alcMenu = Menu("Alc menu", visible = False)
		alc = 10
		while (alc <= MAX_ALC):
			self.alcMenu.addOption(MenuItem("alc", str(alc) + " %", alc))
			alc = alc + 10
		self.alcMenu.addOption(Back("Abrechen"))
		self.alcMenu.setParent(m)

		m.addOptions(drink_opts)
		m.addOption(configuration_menu)
		m.addOption(self.amountMenu)
		m.addOption(self.alcMenu)
		# create a menu context
		self.menuContext = MenuContext(m, self)

	def filterDrinks(self, menu):
		"""
		Removes any drinks that can't be handled by the valve configuration
		"""
		for i in menu.options:
			if (i.type == "drink"):
				i.visible = False
				ingredients = i.attributes["ingredients"]
				presentIng = 0
				for ing in ingredients:
					for p in self.valve_configuration.keys():
						if (ing == self.valve_configuration[p]["value"]):
							presentIng += 1
				if (presentIng == len(ingredients)): 
					i.visible = True
			elif (i.type == "menu"):
				self.filterDrinks(i)

	def selectConfigurations(self, menu):
		"""
		Adds a selection star to the valve configuration option
		"""
		for i in menu.options:
			if (i.type == "valve_selection"):
				key = i.attributes["key"]
				if (self.valve_configuration[key]["value"] == i.attributes["value"]):
					i.name = "%s %s" % (i.attributes["name"], "*")
				else:
					i.name = i.attributes["name"]
			elif (i.type == "menu"):
				self.selectConfigurations(i)

	def prepareForRender(self, menu):
		self.filterDrinks(menu)
		self.selectConfigurations(menu)
		return True

	def menuItemClicked(self, menuItem):
		if (self.lastCallTime + RIGHT_PIN_BOUNCE_SEC < time.time()):
                         # remember time in order to not call the method to often
			self.lastCallTime = time.time()
			print (menuItem.type)
			if (menuItem.type is "drink"):
				self.menuContext.setMenu(self.amountMenu)
				self.selectedDrink = menuItem
				return True
			elif(menuItem.type is "valve_selection"):
				self.valve_configuration[menuItem.attributes["key"]]["value"] = menuItem.attributes["value"]
				Bartender.writeValveConfiguration(self.valve_configuration)
				return True
			elif(menuItem.type is "clean"):
				self.clean()
				return True
			elif(menuItem.type is "amount"):
				self.menuContext.setMenu(self.alcMenu)
				self.selectedAmount = menuItem.attributes
				return True
			elif(menuItem.type is "alc"):
				self.selectedAlcShare = menuItem.attributes
				self.makeDrink(self.selectedDrink.name, self.selectedDrink.attributes)
				return False
			else:
				return False
								
		else:
			return False

	def clean(self):
		waitTime = 20
		valveThreads = []

		# cancel any button presses while the drink is being made
		# self.stopInterrupts()
		self.running = True

		self.lightning.drinkAnimation()
		
		# set time for all threads to be finished
		endTime = time.time() + waitTime
		endTimeFlush = endTime + 0.8
		x = 0

		valveThreads = []		
		for valve in self.valve_configuration.keys():
			if (valve != "air_valve"):
				valveThread = threading.Thread(target = self.cleanValve, args = (self.valve_configuration[valve]["pin"], time.time() + x))
				valveThreads.append(valveThread)
				x = x + 4
				print (valve)
				
		flushThread = threading.Thread(target = self.cleanValve, args = (self.valve_configuration["air_valve"]["pin"], endTimeFlush))
		pumpThread = threading.Thread(target = self.pour, args = (endTimeFlush,))
		progressThread = threading.Thread(target = self.showProgressHint, args = (endTime, True))

		# start pump thread
		pumpThread.start()

		# start the progress bar
		progressThread.start()
		
		# start the valve threads
		for thread in valveThreads:
			thread.start()
			thread.join()

		

		

		print ("Started cleaning")
				
		# wait for threads to finish
		#for thread in valveThreads:
			#thread.join()

		# thread to pump the rest fluid out of the tube
		flushThread.start()

		pumpThread.join()
		flushThread.join()
		progressThread.join()

		self.lightning.stopDrinkAnimation()

		# reenable interrupts
		# self.startInterrupts()
		
		self.running = False

	def cleanValve(self, pin, endTime):
		GPIO.output(pin, GPIO.LOW)
		while (time.time() < endTime  and not self.cancelDrink):
			time.sleep(0.5)
		GPIO.output(pin, GPIO.HIGH)
		print ("Valves finished") 

	def displayMenuItem(self, menuItem):
		print (menuItem.name)
		self.clearDisplay()

                # show headings
		if (menuItem.type == "amount"):
			self.draw.text((self.paddingX, 0), "Fuellmenge:", font=self.font, fill=1)
		elif (menuItem.type == "alc"):
			self.draw.text((self.paddingX, 0), "Alkohol:", font=self.font, fill=1)

		# show specific text			  
		self.draw.text((self.paddingX, self.paddingOneLineY),  menuItem.name,  font=self.font, fill=1)
		self.refreshDisplay()
		
	def processValves(self, valveTimes):
		for valveTime in valveTimes:
			pin = valveTime[0]
			endTimeValve = time.time() + valveTime[1]
			GPIO.output(pin, GPIO.LOW)
			while (time.time() < endTimeValve and not self.cancelDrink):
				time.sleep(0.5)
			GPIO.output(pin, GPIO.HIGH)

		# pump left fluid out of the tube
		airValvePin = self.valve_configuration["air_valve"]["pin"]
		GPIO.output(airValvePin, GPIO.LOW)
		time.sleep(0.8)
		GPIO.output(airValvePin, GPIO.HIGH)
		
		print ("Valves finished")

	def pour(self, endTime):
		GPIO.output(pump["pin"], GPIO.LOW)
		while (time.time() < endTime and not self.cancelDrink):
			time.sleep(0.5)

		# extra time to pump left water out of the tube
		time.sleep(0.8)
		
		GPIO.output(pump["pin"], GPIO.HIGH)
		print("Pump finished")
				
	def showProgressHint(self, endTime, clean):
		#for progress in range(0, 100):
			#self.clearDisplay()
			#self.draw.text((self.paddingX, self.paddingOneLineY), str(progress) + " %", font = self.font, fill = 1)
			#self.refreshDisplay()
		counter = 0
		firstLine = ""
		if (clean):
			firstLine = "Die Ploerre"
		else:
			firstLine = "Dein Drink"
						
		while (time.time() < endTime and not self.cancelDrink):
			
			dots = counter % 4
			dotsString = ""
			for dots in range(0, dots):
				dotsString += ". "
												
			self.clearDisplay()
			self.draw.text((self.paddingX, self.paddingThreeLinesY), firstLine, font = self.font, fill = 1)
			self.draw.text((self.paddingXMiddle, self.paddingThreeLinesY + self.fontSize), "wird", font = self.font, fill = 1)
			self.draw.text((self.paddingX, self.paddingThreeLinesY + 2 * self.fontSize), "gemixt " + dotsString, font = self.font, fill = 1)
			self.refreshDisplay()
			counter += 1
			time.sleep(0.5)			
		print ("Progresshint finished")
				
	def makeDrink(self, drink, attributes):
		# cancel any button presses while the drink is being made
		#  self.stopInterrupts()
		self.running = True

		self.lightning.drinkAnimation()

		# Parse the drink ingredients set up valves and wait times for each valve
		ingredients = attributes["ingredients"];
		accTime = 0
		valveTimes = []
		amountFactor = self.selectedAmount / 100
		for ing in ingredients:
			for valve in self.valve_configuration.keys():
					if valve != "air_valve" and ing == self.valve_configuration[valve]["value"]:
                                                # calculate share of this ingredient
                                                # depends on wether it's alcoholic and how many other (non-) alcoholic ingredients  are in the drink
						alcFactor = 1
						for opt in drink_options:
							if (opt["value"] == ing):
								if (opt["alc"]):
									alcFactor = self.selectedAlcShare / attributes["alcIngCount"]
								else:
									alcFactor = (100 - self.selectedAlcShare) / (len(ingredients) - attributes["alcIngCount"])									

                                                # set tuple for every valve that's involved in the drink
                                                # with (valve_pin, time_to_be_opened)
						valveTime = (self.valve_configuration[valve]["pin"], alcFactor * FLOW_RATE * amountFactor)
						valveTimes.append(valveTime)
						accTime += valveTime[1]
		print (valveTimes)
		print (accTime)
		# set time for all threads to be finished
		endTime = time.time() + accTime

		# create threats
		valveThread = threading.Thread(target = self.processValves, args = (valveTimes, ))
		pumpThread = threading.Thread(target = self.pour, args = (endTime,))
		progressThread = threading.Thread(target = self.showProgressHint, args = (endTime, False))

		#start the pump
		pumpThread.start()		
																																		
		# start the valve thread
		valveThread.start()

		# start the progress hint thread
		progressThread.start()

		# wait for threads to finish
		valveThread.join()
		pumpThread.join()
		progressThread.join()

		self.lightning.stopDrinkAnimation()

		# reenable interrupts
		# self.startInterrupts()
		self.running = False

	def left_btn(self, ctx):
		if not self.running:
			#self.lightning.buttonPressAnimation()
			self.menuContext.advance()                        

	def right_btn(self, ctx):
		if (self.lastCallTime + RIGHT_PIN_BOUNCE_SEC < time.time()):
			#self.lightning.buttonPressAnimation()

			if (not self.running):
				drinkThread = threading.Thread(target = self.menuContext.select)
				drinkThread.start()
			else:
				print ("drink canceled")
		
	def clearDisplay(self):
		self.draw.rectangle((0, 0, self.disp.width, self.disp.height), outline=0, fill=0)
		
	def refreshDisplay(self):
		self.disp.image(self.image)
		self.disp.display()

	def run(self):
		self.startInterrupts()
		# main loop
		try:  
			while True:
				time.sleep(0.1)
		  
		except KeyboardInterrupt:  
			GPIO.cleanup()   # clean up GPIO on CTRL+C exit
			self.clearDisplay()
			self.refreshDisplay()
		GPIO.cleanup()           # clean up GPIO on normal exit
		self.clearDisplay()
		self.refreshDisplay()
		traceback.print_exc()


bartender = Bartender()
bartender.buildMenu(drink_list, drink_options)
bartender.run()





